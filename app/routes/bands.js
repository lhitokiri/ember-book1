import Ember from 'ember'
import Band from '../models/band'
import Song from '../models/song'

export default Ember.Route.extend({
  model: function() {
    var blackDog = Song.create({
      title: 'Black Dong',
      band: 'Zepelin',
      rating: 3
    })

    var quintoTeletubi = Song.create({
      title: 'El Quinto Teletubi',
      band: 'Chabelos',
      rating: 3
    })

    var muchachoProvinciano = Song.create({
      title: 'Muchacho provinciano',
      band: 'Chacalon',
      rating: 3
    })

    var rewrite = Song.create({
      title: 'Rewrite',
      band: 'Asian Kung-fu Generation',
      rating: 3
    })

    var chabelos = Band.create({
      name: 'Chabelos',
      description: 'Chabelos ps',
      songs: [quintoTeletubi, blackDog]
    })

    var chacalon = Band.create({
      name: 'Chacalon',
      description: 'El mejor cantante de musica Chicha',
      songs: [muchachoProvinciano]
    })

    var asianKungFuGeneration = Band.create({
      name: 'Asian KungFu Generation',
      description: 'El mejor ending de Full Metal Alchemist',
      songs: [rewrite]
    })

    return [chabelos, chacalon, asianKungFuGeneration]
  },
  actions: {
    createBand: function() {
      var name = this.get('controller').get('name')
      var band = Band.create({ name: name })
      this.modelFor('bands').pushObject(band)
      this.get('controller').set('name', '')
      this.transitionTo('bands.band.songs', band)
    }
  }
})
